#!/bin/bash

function zzHELP(){
    zzhelp
}

# song utilities
function zzplay(){
    xmms2 play
}

function zzpause(){
    xmms2 pause
}

function zzstop(){
    xmms2 stop
}

function zzlss(){
    xmms2 ls
}

function zzjump(){
    xmms2 jump $1
}

function zznext(){
    xmms2 next
}

function zzprev(){
    xmms2 prev
}

# Playlist management
function zzlsp(){
    xmms2 playlist list
}

function zzswitchp(){
    xmms2 playlist switch $1
}

function zzaddp(){
    xmms2 playlist create $1
}

function zzremovep(){
    xmms2 playlist remove $1
}

function zzadds(){
    xmms2 add $@
}

function zzremoves(){
    xmms2 remove $@
}

function zzhelp() {
    echo "functions available:"
    typeset -f \
    | awk '/zzHELP/{flag=1;next}/zzhelp/{flag=0}flag' \
    | awk '/ \(\) $/ && !/^main / {print $1}' \
    | xargs -n 1 echo "  "
}
