; add column marker
(add-to-list 'load-path "/home/jemmons/.emacs.d/lisp")
(require 'fill-column-indicator)
(require 'python-mode)

(setq fci-rule-color "white")
(setq fci-rule-width 1)
(setq fci-rule-column 70)
(setq-default fill-column 70) 

(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode 1)

; add column numbering
(setq column-number-mode t)

; color highlighting
(load-theme 'tango-dark)

; git gutter
(require 'git-gutter)
(global-git-gutter-mode +1)
(custom-set-variables
 '(git-gutter:update-interval 1))

; always follow symlinks
(setq vc-follow-symlinks t)

; resize windows quickly
(defun win-resize-top-or-bot ()
    "Figure out if the current window is on top, bottom or in the
middle"
    (let* ((win-edges (window-edges))
           (this-window-y-min (nth 1 win-edges))
           (this-window-y-max (nth 3 win-edges))
           (fr-height (frame-height)))
      (cond
       ((eq 0 this-window-y-min) "top")
       ((eq (- fr-height 1) this-window-y-max) "bot")
       (t "mid"))))

(defun win-resize-left-or-right ()
    "Figure out if the current window is to the left, right or in the
middle"
    (let* ((win-edges (window-edges))
           (this-window-x-min (nth 0 win-edges))
           (this-window-x-max (nth 2 win-edges))
           (fr-width (frame-width)))
      (cond
       ((eq 0 this-window-x-min) "left")
       ((eq (+ fr-width 4) this-window-x-max) "right")
       (t "mid"))))

(defun win-resize-enlarge-horiz ()
  (interactive)
  (cond
   ((equal "top" (win-resize-top-or-bot)) (enlarge-window -20))
   ((equal "bot" (win-resize-top-or-bot)) (enlarge-window 20))
   ((equal "mid" (win-resize-top-or-bot)) (enlarge-window -20))
   (t (message "nil"))))

(defun win-resize-minimize-horiz ()
  (interactive)
  (cond
   ((equal "top" (win-resize-top-or-bot)) (enlarge-window 20))
   ((equal "bot" (win-resize-top-or-bot)) (enlarge-window -20))
   ((equal "mid" (win-resize-top-or-bot)) (enlarge-window 20))
   (t (message "nil"))))

(defun win-resize-enlarge-vert ()
  (interactive)
  (cond
   ((equal "left" (win-resize-left-or-right)) (enlarge-window-horizontally -20))
   ((equal "right" (win-resize-left-or-right)) (enlarge-window-horizontally 20))
   ((equal "mid" (win-resize-left-or-right)) (enlarge-window-horizontally -20))))

(defun win-resize-minimize-vert ()
  (interactive)
  (cond
   ((equal "left" (win-resize-left-or-right)) (enlarge-window-horizontally 20))
   ((equal "right" (win-resize-left-or-right)) (enlarge-window-horizontally -20))
   ((equal "mid" (win-resize-left-or-right)) (enlarge-window-horizontally 20))))

(global-set-key (kbd "C-x <down>") 'win-resize-enlarge-vert)
(global-set-key (kbd "C-x <up>") 'win-resize-minimize-vert)
;(global-set-key [C-M-left] 'win-resize-enlarge-vert)
;(global-set-key [C-M-right] 'win-resize-minimize-vert)

(global-set-key (kbd "C-c <up>") 'win-resize-enlarge-horiz)
(global-set-key (kbd "C-c <down>") 'win-resize-minimize-horiz)
;(global-set-key [C-M-up] 'win-resize-enlarge-horiz)
;(global-set-key [C-M-down] 'win-resize-minimize-horiz)

(global-set-key (kbd "C-c +") 'balance-windows)

; python3 mode
(setq py-python-command "python3")

; rust syntax highlighting
(add-to-list 'load-path "/home/jemmons/.emacs.d/lisp/rust-mode.el")
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

; matlab syntax highlighting
(autoload 'matlab-mode "matlab" "Enter MATLAB mode." t)
(setq auto-mode-alist (cons '("\\.m\\'" . matlab-mode) auto-mode-alist))
(autoload 'matlab-shell "matlab" "Interactive MATLAB mode." t)

; make cuda files have c++ highlighting
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cuh\\'" . c++-mode))

; fortran syntax highlighting
(add-to-list 'auto-mode-alist '("\\.F90\\'" . fortran-mode))

; make Rnw files have tex highlighting
(add-to-list 'auto-mode-alist '("\\.Rnw\\'" . tex-mode))

; add ocaml highlighting
;(load "/home/jemmons/.emacs.d/lisp/tuareg/tuareg-site-file")

; reformat paragraph
(global-set-key (kbd "C-x p") 'fill-paragraph) ; Ctrl-x p

; spell check text
(global-set-key (kbd "<f7>") 'flyspell-buffer) ; <f7>
(global-set-key (kbd "<f8>") 'ispell-word) ; <f8>

; comment out selected region
(global-set-key (kbd "C-x /") 'comment-region) ; Ctrl-x /

; uncomment out selected region
(global-set-key (kbd "C-x ?") 'uncomment-region) ; Ctrl-x ?

; set the default tab size to 4 spaces
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)

; remove backup files
(setq make-backup-files nil)
(put 'scroll-left 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(send-mail-function (quote mailclient-send-it)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
